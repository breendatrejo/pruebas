﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace AplicacionBrenda
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/HorarioDeActividades.xaml",UriKind.Relative));
        }

        private void HyperlinkButton_Click_2(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AccesoriosNecesarios.xaml", UriKind.Relative));
        }

        private void HyperlinkButton_Click_3(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AprendeAlimentarlo.xaml", UriKind.Relative));
        }

        private void HyperlinkButton_Click_4(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/VigilaSuSalud.xaml", UriKind.Relative));
        }

        private void HyperlinkButton_Click_5(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AHacerEjercicio.xaml", UriKind.Relative));
        }

        private void HyperlinkButton_Click_6(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/BuenosModales.xaml", UriKind.Relative));
        }

        private void HyperlinkButton_Click_7(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/PerroLimpio.xaml", UriKind.Relative));
        }

       
    }
}